package com.example.springboot_activiti_demo.activiti.controller;

import com.example.springboot_activiti_demo.activiti.Entity.response.RespEntity;
import com.example.springboot_activiti_demo.activiti.service.WorkFlowService;
import com.example.springboot_activiti_demo.activiti.util.JsonUtils;
import com.terran4j.commons.api2doc.annotations.Api2Doc;
import com.terran4j.commons.api2doc.annotations.ApiComment;
import org.activiti.engine.IdentityService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Mingfang Zhu
 * @Description:  提供流程接口
 * @Date: Created in 09:20 2018/5/15
 */

@Api2Doc(id = "workFlow", name = "工作流引擎接口")
@ApiComment(seeClass = RespEntity.class)
@RestController
@RequestMapping(value = "/workFlow")
public class WorkflowController {

    @Autowired
    private IdentityService identityService;

    @Autowired
    private WorkFlowService workFlowService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    /**
     * 启动流程实例入口
     * @param assigneeKey  发起人key
     * @param assigneeId  发起人id
     * @param nextAssigneeKey   下一节点代理人key
     * @param nextAssigneeId   下一节点代理人id
     * @param businessKey   业务主键
     * @param processKey  流程定义key
     * @param parmKey  流程变量key
     * @param parmVal  流程变量value
     */
    @Api2Doc(order = 1)
    @ApiComment("创建流程实例接口")
    @RequestMapping(value = "/startWF",method = {RequestMethod.POST,RequestMethod.GET})
    public RespEntity startWorkFlow(
            @ApiComment(value="流程定义key",sample = "myProcess") @RequestParam(value = "processKey",required = true) String processKey,
            @ApiComment(value = "业务主键",sample = "WF00000001") @RequestParam(value = "businessKey",required = true) String businessKey,
            @ApiComment(value = "发起人key",sample = "act_initiator") @RequestParam(value = "assigneeKey",required = true) String assigneeKey,
            @ApiComment(value = "发起人id",sample = "9320052") @RequestParam(value = "assigneeId",required = true)  String assigneeId,
            @ApiComment(value = "下一节点代理人key",sample ="branch_manager") @RequestParam(value = "nextAssigneeKey",required = true)  String nextAssigneeKey,
            @ApiComment(value = "下一节点代理人id",sample = "9320161") @RequestParam(value = "nextAssigneeId",required = true)   String nextAssigneeId,
            @ApiComment(value = "流程变量参数key",sample = "org_type") @RequestParam(value = "parmKey",required = false)   String parmKey,
            @ApiComment(value = "流程变量参数value",sample = "branch") @RequestParam(value = "parmVal",required = false)   String parmVal
    ) throws IOException {
        //流程发起前设置发起人，记录在流程历史中
        identityService.setAuthenticatedUserId(assigneeId);
        Map<String, Object> assigneeVars = new HashMap<String, Object>();
        assigneeVars.put(assigneeKey,assigneeId);
        Map<String,Object> nextAssigneeVars = new HashMap<String,Object>();
        if (StringUtils.isNotEmpty(parmKey)&&StringUtils.isNotEmpty(parmVal)){
            nextAssigneeVars.put(parmKey,parmVal);
        }
        nextAssigneeVars.put(nextAssigneeKey,nextAssigneeId);
        RespEntity respEntity = workFlowService.startWorkFlow(businessKey,assigneeVars,nextAssigneeVars,processKey);
        JsonUtils.jsonpFormat(request,response, respEntity);
        return null;
    }

    /**
     * 待办任务
     * @Description:此接口会返回待办任务的业务主键集合
     * @param processKey  流程定义key
     * @param assigneeId   当前代理人id
     */
    @Api2Doc(order = 2)
    @ApiComment("待办任务接口，此接口会返回待办任务的业务主键集合")
    @RequestMapping(value = "/queryToDoTaskList",method = RequestMethod.GET)
    public RespEntity queryToDoTaskList(
            @ApiComment(value = "流程定义key",sample = "myProcess") @RequestParam(value = "processKey",required = true) String processKey,
            @ApiComment(value = "节点人id",sample = "9320161") @RequestParam(value = "assigneeId",required = false) String assigneeId
    ) throws IOException {
        //通过流程定义key和userId查询待办任务，以集合形式保存在反馈实体类的data属性中
        RespEntity respEntity = workFlowService.queryToDoTask(processKey,assigneeId);
        JsonUtils.jsonpFormat(request,response,respEntity);
        return null;
    }

    /**
     * 审批任务（下一节点为单实例userTask或end）
     * @param processKey  流程定义key
     * @param parmKey  流程变量key
     * @param parmVal  流程变量value
     * @param businessKey 业务主键D
     * @param businessKey 业务主键D
     * @param nextAssigneeKey 下一节点代理人key
     * @param nextAssigneeId 下一节点代理人id
     */
    @Api2Doc(order = 3)
    @ApiComment("审批任务接口(下一节点为单实例userTask或end)")
    @RequestMapping(value = "/approveTask",method = {RequestMethod.PUT,RequestMethod.GET})
    public RespEntity approveTask(
            @ApiComment(value = "流程定义key",sample = "myProcess") @RequestParam(value = "processKey",required = true) String processKey,
            @ApiComment(value = "流程变量参数key",sample = "org_type") @RequestParam(value = "parmKey",required = false) String parmKey,
            @ApiComment(value = "流程变量参数value",sample = "branch") @RequestParam(value = "parmVal",required = false) String parmVal,
            @ApiComment(value = "业务主键",sample = "WF00000001") @RequestParam(value = "businessKey",required = true) String businessKey,
            @ApiComment(value = "当前节点人id",sample = "9320052") @RequestParam(value = "assigneeId",required = true) String assigneeId,
            @ApiComment(value = "下一节点代理人key",sample = "branch_manager") @RequestParam(value = "nextAssigneeKey",required = false) String nextAssigneeKey,
            @ApiComment(value = "下一节点代理人id",sample = "9320161") @RequestParam(value = "nextAssigneeId",required = false) String nextAssigneeId
    ) throws IOException {
        Map<String, Object> variables = new HashMap<String, Object>();
        //根据业务参数设置流程变量参数
        if (StringUtils.isNotEmpty(parmKey) && StringUtils.isNotEmpty(parmVal)) {
            variables.put(parmKey, parmVal);
        }
        if (StringUtils.isNotEmpty(nextAssigneeKey) && StringUtils.isNotEmpty(nextAssigneeId)) {
            variables.put(nextAssigneeKey, nextAssigneeId);
        }
        RespEntity respEntity = workFlowService.approveTask(processKey,businessKey,variables,assigneeId);
        JsonUtils.jsonpFormat(request,response,respEntity);
        return null;

    }

    /**
     * 审批任务（下一节点为多实例userTask）
     * @param processKey  流程定义key
     * @param parmKey  流程变量key
     * @param parmVal  流程变量value
     * @param businessKey 业务主键D
     * @param nextAssigneeKey 下一节点代理人key
     * @param nextAssigneeIdList 下一节点代理人id集合
     */
    @Api2Doc(order = 4)
    @ApiComment("审批任务接口（下一节点为多实例userTask）")
    @RequestMapping(value = "/approveTaskMult",method = {RequestMethod.PUT,RequestMethod.GET})
    public RespEntity approveTaskMult(
            @ApiComment(value = "流程定义key",sample = "myProcess") @RequestParam(value = "processKey",required = true) String processKey,
            @ApiComment(value = "流程变量参数key",sample = "org_type") @RequestParam(value = "parmKey",required = false) String parmKey,
            @ApiComment(value = "流程变量参数value",sample = "branch") @RequestParam(value = "parmVal",required = false) String parmVal,
            @ApiComment(value = "业务主键",sample = "WF00000001") @RequestParam(value = "businessKey",required = true) String businessKey,
            @ApiComment(value = "当前节点人id",sample = "9320033") @RequestParam(value = "assigneeId",required = true) String assigneeId,
            @ApiComment(value = "下一节点代理人key",sample = "accountant_officer_list") @RequestParam(value = "nextAssigneeKey",required = true) String nextAssigneeKey,
            @ApiComment(value = "下一节点代理人id",sample = "9320161,9320151,9320141") @RequestParam(value = "nextAssigneeIdList",required = true) String nextAssigneeIdList
    ) throws IOException {
        Map<String, Object> variables = new HashMap<String, Object>();
        if (StringUtils.isNotEmpty(parmKey)&&StringUtils.isNotEmpty(parmVal)) {
            variables.put(parmKey, parmVal);
        }
        List<String> nextAssigneeIds = Arrays.asList(nextAssigneeIdList.split(","));
        variables.put(nextAssigneeKey,nextAssigneeIds);
        RespEntity respEntity = workFlowService.approveTask(processKey,businessKey,variables,assigneeId);
        JsonUtils.jsonpFormat(request,response,respEntity);
        return null;
    }

    /**
     * 提前终止流程
     * @param processKey 流程定义key
     * @param businessKey 业务主键
     * @return
     * @throws IOException
     */
    @Api2Doc(order = 5)
    @ApiComment("提前终止流程")
    @RequestMapping(value = "/terminateWorkFlow",method = {RequestMethod.DELETE,RequestMethod.GET})
    public RespEntity terminateWorkFlow(
            @ApiComment(value = "流程定义key",sample = "myProcess") @RequestParam(value = "processKey",required = true) String processKey,
            @ApiComment(value = "业务主键",sample = "WF00000001") @RequestParam(value = "businessKey",required = true) String businessKey
    ) throws IOException {
        RespEntity respEntity = workFlowService.terminateWorkFlow(processKey,businessKey);
        JsonUtils.jsonpFormat(request,response,respEntity);
        return null;
    }

    /**
     * 查询历史审批列表
     * @param businessKey
     * @param assigneId
     * @return
     */
    @Api2Doc(order = 6)
    @ApiComment("查询历史审批节点列表")
    @RequestMapping(value = "/queryHistoryTaskList",method = RequestMethod.GET)
    public RespEntity queryHistoryTaskList(
            @ApiComment(value = "业务主键",sample = "WF00000001") @RequestParam(value = "businessKey",required = true) String businessKey,
            @ApiComment(value = "用户id",sample = "9320052") @RequestParam(value = "assigneId",required = false) String assigneId
    ) throws IOException {
        RespEntity respEntity = workFlowService.queryHistoryTaskList(businessKey, assigneId);
        JsonUtils.jsonpFormat(request,response,respEntity);
        return null;
    }

    @Api2Doc(order = 7)
    @ApiComment("追踪流程图")
    @RequestMapping(value = "/traceFlowDiagram",method = RequestMethod.GET)
    public RespEntity traceFlowDiagram(
            @ApiComment(value = "流程定义key",sample = "myProcess") @RequestParam(value = "processKey",required = true) String processKey,
            @ApiComment(value = "业务主键",sample = "WF00000001") @RequestParam(value = "businessKey",required = true) String businessKey
    ) throws IOException {
        RespEntity respEntity = workFlowService.traceFlowDiagram(response,processKey,businessKey);
        return respEntity;
    }

    /*TODO:撤回*/
    @Api2Doc(order = 8)
    @ApiComment("流程撤回")
    @RequestMapping(value = "/withdrawTask",method = {RequestMethod.POST,RequestMethod.GET})
    public RespEntity withdrawTask(
            @ApiComment(value = "流程定义key",sample = "myProcess") @RequestParam(value = "processKey",required = true) String processKey,
            @ApiComment(value = "业务主键",sample = "WF00000001") @RequestParam(value = "businessKey",required = true) String businessKey
    ){
            RespEntity respEntity = workFlowService.withdraw(processKey,businessKey);
        return null;
    }




}

