package com.example.springboot_activiti_demo.activiti.util;

import java.util.HashMap;
import java.util.List;

public class LayuiDataUtil extends HashMap<String, Object> {

    /**
     * 分页
     */
    public static HashMap<String, Object> prePageData(HashMap<String, Object> params,int totol){
        String str = String.valueOf(params.get("limit"));
        int limit = Integer.parseInt(str);
        params.put("limit",limit);
        str = String.valueOf(params.get("page"));
        int page = Integer.parseInt(str);
        if(page==0){page=1;}
        int offset = (page - 1) * limit;
        while (totol <offset) {
            offset -= limit;
            page--;
        }
        params.put("offset",offset);

        return params;
    }


    /**
     * 组装layui table所需数据
     */
    public static LayuiDataUtil preData(Integer count, List<?> list){
        LayuiDataUtil data = new LayuiDataUtil();
        data.put("code", 0);
        data.put("msg", "");
        data.put("count", count);
        data.put("data", list);
        return data;
    }
}