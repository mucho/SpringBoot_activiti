package com.example.springboot_activiti_demo.activiti.util;

/**
 * @Author: Mingfang Zhu
 * @Description:
 * @Date: Created in 14:53 2018/6/9
 */
public class SysConstant {

    /**
     * 反馈信息标志
     */
    public static final int SUCCESS_STATUS = 0;
    public static final int FAIL_STATUS = 1;


}
