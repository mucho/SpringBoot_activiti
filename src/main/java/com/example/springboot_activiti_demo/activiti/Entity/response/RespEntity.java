package com.example.springboot_activiti_demo.activiti.Entity.response;

import com.terran4j.commons.api2doc.annotations.ApiComment;

import java.util.List;

/**
 * @Author: Mingfang Zhu
 * @Description:接口返回信息类
 * @Date: Created in 22:34 2018/5/10
 */
public class RespEntity {

    /**
     * 状态 0代表成功、1代表失败
     */
    @ApiComment(value = "状态标志， 0代表成功、1代表失败", sample = "0")
    private int status;

    /**
     * 反馈信息
     */
    @ApiComment(value = "反馈信息", sample = "流程实例操作成功！")
    private String msg;

    /**
     * 返回数据
     */
    @ApiComment(value = "返回数据",sample = "WF00000001,WF00000002,WF00000003")
    private Object data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
