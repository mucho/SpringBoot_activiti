package com.example.springboot_activiti_demo;

import com.terran4j.commons.api2doc.config.EnableApi2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//  文档访问地址： http://localhost:8080/api2doc/home.html
@EnableApi2Doc
@SpringBootApplication
//@ComponentScan("org.activiti.rest")
public class SpringbootActivitiDemoApplication extends SpringBootServletInitializer {
//public class SpringbootActivitiDemoApplication {



	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {

		return builder.sources(SpringbootActivitiDemoApplication.class);

	}



	public static void main(String[] args) {
		SpringApplication.run(SpringbootActivitiDemoApplication.class, args);
	}




}
