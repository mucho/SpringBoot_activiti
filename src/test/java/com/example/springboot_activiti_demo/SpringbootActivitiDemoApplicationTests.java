package com.example.springboot_activiti_demo;

import com.example.springboot_activiti_demo.activiti.service.WorkFlowService;
//import com.example.springboot_activiti_demo.activiti.service.impl.ActivityDemoServiceImpl;
import org.activiti.engine.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


//这是JUnit的注解，通过这个注解让SpringJUnit4ClassRunner这个类提供Spring测试上下文。
//@RunWith(SpringJUnit4ClassRunner.class)
//这是Spring Boot注解，为了进行集成测试，需要通过这个注解加载和配置Spring应用上下
//@SpringBootTest
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootActivitiDemoApplicationTests {



//		@Resource
//		private ActivityDemoServiceImpl activityDemoServiceImpl;
		@Autowired
		private TaskService taskService;
		@Autowired
		private HistoryService historyService;

		@Autowired
		private ProcessEngine processEngine;

		@Autowired
		private IdentityService identityService;

		@Autowired
		private RuntimeService runtimeService;


		@Autowired
		private RepositoryService repositoryService;

		@Autowired
		WorkFlowService workFlowService;


		//测试启动流程接口
		@Test
		public void testCountersign(){

//			String businessKey = "2";
//			Map<String, Object> assigneeVars = new HashMap<String, Object>();
//			assigneeVars.put("act_initiator","活动营销发起人");
//			Map<String,Object> nextAssigneeVars = new HashMap<String,Object>();
//
//			nextAssigneeVars.put("general_science_manager","总行科技经理");
//			nextAssigneeVars.put("org_type","general");
//			String processKey = "precision_marketing";
//			RespEntity respEntity = leaveWorkFlowService.startWorkFlow(businessKey,assigneeVars,nextAssigneeVars,processKey);
//			System.out.println(respEntity);
		}
//
//		//待办任务查询
//		@Test
//		public void testTodoWFlist(){
//			String processKey = "precision_marketing";
//			String assignee = "zxc";
//			RespEntity respEntity = leaveWorkFlowService.queryToDoTask(processKey,assignee);
////			List<Task> resultTask = taskService.createTaskQuery().processDefinitionKey(processKey).taskCandidateOrAssigned(assignee).list();
//			System.out.println(respEntity);
//		}
//
//
//	@Test
//		//审批任务
//		public void testApproveTask(){
//			String processKey = "precision_marketing";
//			String businessKey = "2";
//			String assignee = "aa";
//			Map<String,Object> variables = new HashMap<String,Object>();
//
//			leaveWorkFlowService.approveTask(processKey,businessKey,variables,assignee);
//		}



		//测试会签任务
//		@Test
//		public void testApproveCountersign(){
//			String processKey = "precision_marketing";
//			List<String> assigneeList = new ArrayList<>();
//			assigneeList.add("aa");
//			assigneeList.add("ss");
//			assigneeList.add("dd");
//			String assignee = "总行负责人";
//			Map<String,Object> variables = new HashMap<String,Object>();
//			variables.put("accountant_officer_list",assigneeList);
//			String businessKey = "2";
// 			RespEntity respEntity = leaveWorkFlowService.approveTask(processKey,businessKey,variables,assignee);
//		}

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//		@Test
//		public void terminateWorkFlow() {

//			List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery().processDefinitionKey("precision_marketing").processInstanceBusinessKey("1").list();
//			if (processInstanceList != null && processInstanceList.size() > 0) {
//				String processInstanceId = processInstanceList.get(0).getProcessInstanceId();
//				System.out.println(processInstanceId);
//				runtimeService.deleteProcessInstance(processInstanceId,"删除测试");
//			}

//		}


//}

//
//		@Test
//		public void getTask(){
//
//
//			String assignee = "";
////			List<Task> list = taskService.createTaskQuery().taskCandidateUser("qwer").
////
////					orderByTaskCreateTime().desc().list();//查询所拥有的候选任务
//
//			List<Task> list =taskService.createTaskQuery().taskCandidateOrAssigned(assignee)
//
//					.orderByTaskCreateTime().desc().list();//查询所拥有的个人&候选任务
//
//			System.out.println("list:"+list);
////			taskService.claim(taskId, assignee);
////			processEngine.getTaskService().setAssignee("57520", null);//先将指派人赋空值
//		}
//
//		@Test
//		public void mytaskClaimAComplete(){
//
//			taskService.claim("57520", "zzz");
//
//			Map<String,Object>variables = new HashMap();//设置流程变量
//
//			variables.put("managerCheckResult", "同意");
//
//			variables.put("managerCheckreason","可用年休假");
//
//			variables.put("managerTime",new Date());
//
//			taskService.complete("57520",variables);
//
//		}
//
//		//测试流程变量
//		@Test
//		public void startprocessByKey(){
//
//			String key = "myProcess_2";
//
//			String applyuser ="zyq";
//
//			identityService.setAuthenticatedUserId(applyuser);
//
//			Map<String,Object> variables = new HashMap();//设置流程变量
//
//			variables.put("userIds","qwer");
//
//			variables.put("applyTitle","test_请假申请流程");
//
//			variables.put("applyTime","7天");
//
//			variables.put("applyCtreateTime",new Date());
//
//			variables.put("applyReason","休假");
//
//			//设置候选人：上级领导审批
//
//			List list =new ArrayList();
//
//			list.add("zzz");
//
//			list.add("xxx");
//
//			variables.put("managers",list);
//
//			ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(key,variables);
//
//
//
//			Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
//
//			System.out.println(task.getAssignee());
//
//
//		}
//
//		@Test
//		public void queryTask(){
//			//获取任务服务对象
//			TaskService taskService = processEngine.getTaskService();
//			//根据接受人获取该用户的任务
//			List<Task> tasks = taskService.createTaskQuery()
//					.taskAssignee("9320052")
//					.list();
//			for (Task task : tasks) {
//				System.out.println("ID:"+task.getId()+",姓名:"+task.getName()+",接收人:"+task.getAssignee()+",开始时间:"+task.getCreateTime());
//			}
//		}
//
//		//启动流程
//		@Test
//		public void startPro() {
//
//			activityDemoServiceImpl.startProcesses("myProcess_2","leave");
//		}
//
//		//获取受理员任务列表
//		@Test
//		public void findTasksForSL() {
//			List<Task> lists = activityDemoServiceImpl.findTasksByUserId("999000");
//			System.out.println("任务列表："+lists);//任务列表：[Task[id=210028, name=受理], Task[id=215005, name=受理]]
//		}
//
//		//受理员受理数据
//		@Test
//		public void completeTasksForSL() {
//			activityDemoServiceImpl.completeTask("47504", "huyang", "true");//受理后，任务列表数据减少
//		}
//
//		//获取审批员任务列表
//		@Test
//		public void findTasksForSP() {
//			List<Task> lists = activityDemoServiceImpl.findTasksByUserId("hruser");
//			System.out.println("任务列表："+lists);//任务列表：[Task[id=220004, name=审批]]
//		}
//
//		//审批员通过审核
//		@Test
//		public void completeTasksForSP() {
//			activityDemoServiceImpl.completeTask("52504", "hruser", "true");//审批后，任务列表数据减少
//		}
//
//
//		//设置流程变量
//		//设置流程变量【基本类型】
//		@Test
//		public void setTasksVar() {
//			List<Task> lists = activityDemoServiceImpl.findTasksByUserId("sly1");
//			for(Task task : lists) {//不知为何，变量保存成功，但数据表只有请假天数含有任务id，单获取流程变量时，根据任务id均可获取到（如下一测试）
//				taskService.setVariable(task.getId(), "请假人", "sly1");
//				taskService.setVariableLocal(task.getId(), "请假天数",3);
//				taskService.setVariable(task.getId(), "请假日期", new Date());
//			}
//		}
//
//		//获取流程变量
//		@Test
//		public void getTasksVar() {
//			List<Task> lists = activityDemoServiceImpl.findTasksByUserId("huyang");
//			for(Task task : lists) {
//				//获取流程变量【基本类型】
//				String person = (String) taskService.getVariable(task.getId(), "请假人");
//				Integer day = (Integer) taskService.getVariableLocal(task.getId(), "请假天数");
//				Date date = (Date) taskService.getVariable(task.getId(), "请假日期");
//
//				System.out.println("流程变量："+person+"||"+day+"||"+date+"||");
//			}
//		}
//
//		//设置流程变量【实体】
//		@Test
//		public void setTasksVarEntity() {
//			Date date = new Date();
//			List<Task> lists = activityDemoServiceImpl.findTasksByUserId("sly1");
//			for(Task task : lists) {
//
//				Person p = new Person();
//				p.setName("翠花");
//				p.setId(20);
//				p.setDate(date);
//				p.setNote("回去探亲，一起吃个饭123");
//				taskService.setVariable(task.getId(), "人员信息(添加固定版本)", p);
//
//				System.out.println("设置流程变量成功！");
//			}
//		}

		//获取流程变量【实体】  实体必须序列化
//		@Test
//		public void getTasksVarEntity() {
//			List<Task> lists = activityDemoServiceImpl.findTasksByUserId("sly1");
//			for(Task task : lists) {
//				// 2.获取流程变量，使用javaBean类型
//				Person p = (Person)taskService.getVariable(task.getId(), "人员信息(添加固定版本)");
//				System.out.println(" 请假人：  "+p.getName()+"  请假天数：  "+p.getId()+"   请假时间："+ p.getDate()+ "   请假原因： "+p.getNote());
//			}
//		}

//
//		//生成流程图---232501
//		@Test
//		public void queryProImg() throws Exception {
//			activityDemoServiceImpl.queryProImg("232501");
//		}
//
//		//生成流程图（高亮）---232501
//		@Test
//		public void queryProHighLighted() throws Exception {
//			activityDemoServiceImpl.queryProHighLighted("232501");
//		}
//
//		/**
//		 * 查询流程变量的历史表,可以根据变量名称查询该变量的所有历史信息
//		 */
//		@Test
//		public void findHistoryProcessVariables(){
//			List<HistoricVariableInstance> list = historyService.createHistoricVariableInstanceQuery()//创建一个历史的流程变量查询对象
//					.variableName("请假天数")
//					.list();
//			if (list!=null &&list.size()>0) {
//				for (HistoricVariableInstance hvi : list) {
//					System.out.println(hvi.getId()+"     "+hvi.getProcessInstanceId()+"   "+hvi.getVariableName()
//							+"   "+hvi.getVariableTypeName()+"    "+hvi.getValue());
//					System.out.println("########################################");
//				}
//			}
//
//		}
//


//
//	@Test
//	public void queryHistoricTask() {
//		List<HistoricTaskInstance> list = historyService
//				.createHistoricTaskInstanceQuery()
//				.processInstanceBusinessKey("2").taskAssignee("活动营销发起人").list();
//		if (list != null && list.size() > 0) {
//			for (HistoricTaskInstance hti : list) {
//				System.out.print("taskId:" + hti.getId()+"，");
//				System.out.print("name:" + hti.getName()+"，");
//				System.out.print("pdId:" + hti.getProcessDefinitionId()+"，");
//				System.out.print("assignee:" + hti.getAssignee()+"，");
//			}
//		}
//	}
//
//	@Test
//		public void test() {
//		String nextAssigneeIds = "asda";
//		List<String> nextAssigneeIdList = Arrays.asList(nextAssigneeIds.split(","));
//		System.out.println(nextAssigneeIds);
//		List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery().taskAssignee("活动营销发起人").list();//排序
//
//		if (list != null && list.size() > 0) {
//
//			for (HistoricActivityInstance hai : list) {
//				System.out.println(hai.getId());
//				System.out.println("步骤ID：" + hai.getActivityId());
//				System.out.println("步骤名称：" + hai.getActivityName());
//				System.out.println("执行人：" + hai.getAssignee());
//				System.out.println("====================================");
//			}
//
//		}
//	}
//			List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery().
//			processInstanceBusinessKey("2").asc()//排序
//					.list();
//			if (list != null && list.size() > 0) {
//
//				for (HistoricProcessInstance hpi : list) {
//					System.out.println("流程定义ID：" + hpi.getProcessDefinitionId());
//					System.out.println("流程实例ID：" + hpi.getId());
//					System.out.println("开始时间：" + hpi.getStartTime());
//					System.out.println("结束时间：" + hpi.getEndTime());
//					System.out.println("流程持续时间：" + hpi.getDurationInMillis());
//					System.out.println("=======================================");
//				}
//			}
//			// 查询已完成的流程
//			List<HistoricProcessInstance> datas = historyService
//					.createHistoricProcessInstanceQuery().finished().list();
//			datas = historyService.createHistoricProcessInstanceQuery()
//					.processDefinitionKey("precision_marketing").list();
//			datas = historyService.createHistoricProcessInstanceQuery()
//					.processInstanceBusinessKey("WF00000001").list();
//			System.out.println("使用finished方法：" + datas.size());

//		}
//		/**
//		 *  历史流程实例查询
//		 *  http://blog.csdn.net/luckyzhoustar/article/details/48652783
//		 */
//		@Test
//		public void findHistoricProcessInstance() {
//			// 查询已完成的流程
//			List<HistoricProcessInstance> datas = historyService
//					.createHistoricProcessInstanceQuery().finished().list();
//			System.out.println("使用finished方法：" + datas.size());
//			// 根据流程定义ID查询
//			datas = historyService.createHistoricProcessInstanceQuery()
//					.processDefinitionId("processDefinitionId").list();
//			System.out.println("使用processDefinitionId方法： " + datas.size());
//			// 根据流程定义key（流程描述文件的process节点id属性）查询
//			datas = historyService.createHistoricProcessInstanceQuery()
//					.processDefinitionKey("processDefinitionKey").list();
//			System.out.println("使用processDefinitionKey方法： " + datas.size());
//			// 根据业务主键查询
//			datas = historyService.createHistoricProcessInstanceQuery()
//					.processInstanceBusinessKey("processInstanceBusinessKey").list();
//			System.out.println("使用processInstanceBusinessKey方法： " + datas.size());
//			// 根据流程实例ID查询
//			datas = historyService.createHistoricProcessInstanceQuery()
//					.processInstanceId("processInstanceId").list();
//			System.out.println("使用processInstanceId方法： " + datas.size());
//			// 查询没有完成的流程实例
//			historyService.createHistoricProcessInstanceQuery().unfinished().list();
//			System.out.println("使用unfinished方法： " + datas.size());
//		}
//
//		/**
//		 *  历史任务查询
//		 * @throws ParseException
//		 */
//		@Test
//		public void findHistoricTasks() throws ParseException {
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//			//历史数据查询
//			List<HistoricTaskInstance> datas = historyService.createHistoricTaskInstanceQuery()
//					.finished().list();
//			System.out.println("使用finished方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.processDefinitionId("processDefinitionId").list();
//			System.out.println("使用processDefinitionId方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.processDefinitionKey("testProcess").list();
//			System.out.println("使用processDefinitionKey方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.processDefinitionName("testProcess2").list();
//			System.out.println("使用processDefinitionName方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.processFinished().list();
//			System.out.println("使用processFinished方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.processInstanceId("processInstanceId").list();
//			System.out.println("使用processInstanceId方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.processUnfinished().list();
//			System.out.println("使用processUnfinished方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.taskAssignee("crazyit").list();
//			System.out.println("使用taskAssignee方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.taskAssigneeLike("%zy%").list();
//			System.out.println("使用taskAssigneeLike方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.taskDefinitionKey("usertask1").list();
//			System.out.println("使用taskDefinitionKey方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.taskDueAfter(sdf.parse("2020-10-11 06:00:00")).list();
//			System.out.println("使用taskDueAfter方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.taskDueBefore(sdf.parse("2022-10-11 06:00:00")).list();
//			System.out.println("使用taskDueBefore方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.taskDueDate(sdf.parse("2020-10-11 06:00:00")).list();
//			System.out.println("使用taskDueDate方法查询：" + datas.size());
//			datas = historyService.createHistoricTaskInstanceQuery()
//					.unfinished().list();
//			System.out.println("使用unfinished方法查询：" + datas.size());
//		}
//		/**
//		 *  历史行为查询
//		 *  流程在进行过程中，每每走一个节点，都会记录流程节点的信息，包括节点的id，名称、类型、时间等，保存到ACT_HI_ACTINST表中。
//		 * @throws ParseException
//		 */
//		@Test
//		public void findHistoricActivityInstance() {
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//			//查询数据
//			List<HistoricActivityInstance> datas = historyService.createHistoricActivityInstanceQuery()
//					.activityId("endevent1").list();
//			System.out.println("使用activityId查询：" + datas.size());
//			datas = historyService.createHistoricActivityInstanceQuery()
//					.activityInstanceId(datas.get(0).getId()).list();
//			System.out.println("使用activityInstanceId查询：" + datas.size());
//			datas = historyService.createHistoricActivityInstanceQuery()
//					.activityType("intermediateSignalCatch").list();
//			System.out.println("使用activityType查询：" + datas.size());
//			datas = historyService.createHistoricActivityInstanceQuery()
//					.executionId("executionId").list();
//			System.out.println("使用executionId查询：" + datas.size());
//			datas = historyService.createHistoricActivityInstanceQuery().finished().list();
//			System.out.println("使用finished查询：" + datas.size());
//			datas = historyService.createHistoricActivityInstanceQuery()
//					.processInstanceId("processInstanceId").list();
//			System.out.println("使用processInstanceId查询：" + datas.size());
//			datas = historyService.createHistoricActivityInstanceQuery()
//					.taskAssignee("crazyit").list();
//			System.out.println("使用taskAssignee查询：" + datas.size());
//			datas = historyService.createHistoricActivityInstanceQuery().unfinished().list();
//			System.out.println("使用unfinished查询：" + datas.size());
//		}
//
//		/**
//		 *  历史流程明细查询
//		 *  在流程进行的过程中，会产生许多明细数据，只有将History设置为最高级别的时候，才会被记录到ACT_HI_DETAIL表中。
//		 * @throws ParseException
//		 */
//		@Test
//		public void findHistoricDetail() {
//			// 查询历史行为
//			HistoricActivityInstance act = historyService.createHistoricActivityInstanceQuery()
//					.activityName("First Task").finished().singleResult();
//			List<HistoricDetail> datas = historyService.createHistoricDetailQuery()
//					.activityInstanceId(act.getId()).list();
//			System.out.println("使用activityInstanceId方法查询：" + datas.size());
//			datas = historyService.createHistoricDetailQuery().excludeTaskDetails().list();
//			System.out.println("使用excludeTaskDetails方法查询：" + datas.size());
//			datas = historyService.createHistoricDetailQuery().formProperties().list();
//			System.out.println("使用formProperties方法查询：" + datas.size());
//			datas = historyService.createHistoricDetailQuery().processInstanceId("processInstanceId").list();
//			System.out.println("使用processInstanceId方法查询：" + datas.size());
//			datas = historyService.createHistoricDetailQuery().taskId("taskId").list();
//			System.out.println("使用taskId方法查询：" + datas.size());
//			datas = historyService.createHistoricDetailQuery().variableUpdates().list();
//			System.out.println("使用variableUpdates方法查询：" + datas.size());
//		}
//
//
//
//		@Test
//		public void contextLoads() {
//			String userId = "huyang";
//			List<RLeave> results = new ArrayList<RLeave>();
//			List<Task> tasks = new ArrayList<Task>();
//
//			// 根据当前人的ID查询
//			List<Task> todoList = taskService.createTaskQuery().processDefinitionKey("myProcess_2").
//					taskAssignee(userId).list();
//
//			// 根据当前人未签收的任务
//			List<Task> unsignedTasks = taskService.createTaskQuery().processDefinitionKey("myProcess_2").
//					taskCandidateUser(userId).list();
//
//			// 合并
//			tasks.addAll(todoList);
//			tasks.addAll(unsignedTasks);
//			if (tasks.size()>0) {
//				// 根据流程的业务ID查询实体并关联
//				for (Task task : tasks) {
//					String processInstanceId = task.getProcessInstanceId();
//					ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
//					String businessKey = processInstance.getBusinessKey();
//					RLeave rLeave = rLeaveJPA.findByReportNo(businessKey);
//					rLeave.setTask(task);
//					rLeave.setProcessInstance(processInstance);
//					String processDefinitionId = processInstance.getProcessDefinitionId();
//					ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();
//					rLeave.setProcessDefinition(processDefinition);
//					results.add(rLeave);
//				}
//			}
//
//		}


}
